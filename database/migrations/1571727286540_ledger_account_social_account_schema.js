'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LedgerAccountSocialAccountSchema extends Schema {
  up () {
    this.create('ledger_account_social_accounts', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('master_users')
      table.enu('type',['email','phone','web']).default('email').notNullable()
      table.string('value').default('').notNullable()
      table.boolean('active').default(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('ledger_account_social_accounts')
  }
}

module.exports = LedgerAccountSocialAccountSchema
