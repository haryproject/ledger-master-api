'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LedgerAccountTransactionSchema extends Schema {
  up () {
    this.create('ledger_account_transactions', (table) => {
      table.increments()
      table.string('transaction_no')
      table.string('sender')
      table.string('receiver')
      table.double('amount')
      table.enu('source',['token','point'])
      table.enu('type',['deposit','withdraw','transfer','payment'])
      table.timestamps()
    })
  }

  down () {
    this.drop('ledger_account_transactions')
  }
}

module.exports = LedgerAccountTransactionSchema
