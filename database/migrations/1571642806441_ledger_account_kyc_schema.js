'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LedgerAccountKycSchema extends Schema {
  up () {
    this.create('ledger_account_kycs', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('master_users')
      table.string('first_name').notNullable().default('')
      table.string('last_name').notNullable().default('')
      table.string('middle_name').notNullable().default('')
      table.string('phone').notNullable().default('')
      table.date('dob')
      table.enu('sex',['male','female','undefined']).notNullable().default('undefined')
      table.text('address').notNullable().default('')
      table.string('city').notNullable().default('')
      table.string('province').notNullable().default('')
      table.string('nation').notNullable().default('')
      table.string('zip_code').notNullable().default('')
      table.enu('kyc_type',['driving_license','passport','national_id']).notNullable().default('national_id')
      table.text('front_image').default('')
      table.text('back_image').default('')
      table.text('prove_image').default('')
      table.enu('status',['pending','refused-info','refused-front-id','refused-back-id','refused-proof','banned'])
      table.text('note')
      table.timestamps()
    })
  }

  down () {
    this.drop('ledger_account_kycs')
  }
}

module.exports = LedgerAccountKycSchema
