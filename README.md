# Ledger Master API

Documentation for ledger apps that support account in Starx Ecosystem.
This documentation includes :
1. Register Account
2. Login Account
3. User Info
4. KYC Registration
5. Email Validation
6. Phone Validation

## Setup

To setup the project please install all the the npm module

```bash
npm install
```


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```


### Encryption
Update the encryption endpoint on the App/Helper/EncryptionHelper.js the current Encryption provider

```js
module.exports.encrypt = async function (data) {
  let result;
  await axios.post('{{CURRENT_ENV_MASTER_ENDPOINT}}'+apiKey+'/encrypt', {
    data : data
  })
    .then(function (response) {
      result = response.data
      return response.data
    })
    .catch(function (error) {

      return false
    });
  // console.log(result)
  return  result
}
```


### Documentation
Use this following URL to get the documentation 

**Register Account**

`https://documenter.getpostman.com/view/7920071/SW14Uwo9`

**Production**

`https://documenter.getpostman.com/view/7920071/SW15zGvp`

**Sandbox**

`https://documenter.getpostman.com/view/7920071/SW15zGvq`
