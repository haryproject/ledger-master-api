FROM node:10

WORKDIR /usr/src/app

COPY package*.json ./


RUN npm i -g @adonisjs/cli
RUN npm install


COPY . .

EXPOSE 1111

CMD ["adonis","serve" ]