'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Helpers = use('Helpers')

Route.get('/', () => {
  return { greeting: 'Ledger Master API Edited' }
})

Route.get('/:path/:name',({params,response})=>{
  const {name,path} = params
  const tmpPath = Helpers.publicPath('/'+path+'/'+name)
  return response.download(tmpPath)
})



Route.group(()=>{


  Route.get('/kyc','LedgerAccountKycController.show').middleware(['auth','notAdmin'])
  Route.post('/kyc','LedgerAccountKycController.store').middleware('auth').validator('Kyc')
  Route.put('/kyc','LedgerAccountKycController.updateSelf').middleware('auth',).validator('Kyc')
  Route.put('/kyc/:uuid/examine','LedgerAccountKycController.examineKyc').middleware('auth','admin').validator('ExamineKyc')
  Route.put('/kyc/:uuid/unbanned','LedgerAccountKycController.unBanUserAccount').middleware('auth','admin')
  Route.get('/:uuid/kyc','LedgerAccountKycController.showByIndex').middleware('auth','admin')


  Route.get('/info','UserController.info').middleware(['auth','invalid'])

  Route.put('/kyc/image/front','LedgerAccountKycController.uploadFrontImage').middleware('auth').validator('UploadKycImage')
  Route.put('/kyc/image/back','LedgerAccountKycController.uploadBackImage').middleware('auth').validator('UploadKycImage')
  Route.put('/kyc/image/prove','LedgerAccountKycController.uploadProveImage').middleware('auth').validator('UploadKycImage')

  Route.put('/kyc/approve','LedgerAccountKycController.approveKyc').middleware('auth','admin')

  Route.put('/update/email','AccountCheckerController.updateEmail').middleware(['auth']).validator('UpdateEmail')

  Route.get('/request/update/password','AccountCheckerController.requestUpdatePassword').middleware(['auth'])
  Route.post('/update/password','AccountCheckerController.updatePassword').middleware(['auth',]).validator('UpdatePassword')

  Route.post('/update/phone',"AccountCheckerController.updatePhone").middleware(['auth']).validator('UpdatePhone')
  Route.post('/send/confirmation-email','AccountCheckerController.sendEmailConfirmation').middleware(['auth']).validator('EmailConfirmation')

  Route.get('/transaction','LedgerAccountTransactionController.ledgerHistory').middleware(['auth','valid'])
  Route.get('/transaction/:year','LedgerAccountTransactionController.ledgerHistoryYear').middleware(['auth','valid'])
  Route.get('/transaction/:year/:month','LedgerAccountTransactionController.ledgerHistoryMonth').middleware(['auth','valid'])
  Route.get('/transaction/:year/:month/:day','LedgerAccountTransactionController.ledgerHistoryDay').middleware(['auth','valid'])

  Route.get('/count','LedgerAccountKycController.kycCountByStatus').middleware('auth','admin', )

  Route.get('/', 'LedgerAccountKycController.index').middleware('auth','admin', )
  Route.get('/:query', 'LedgerAccountKycController.groupByStatus').middleware('auth','admin',)

}).prefix('api/v1/ledger')
