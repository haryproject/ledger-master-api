'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const CredentialChecker = use('App/Helper/CredentialChecker')
const {Ecosystem, Authority} = use('App/Helper/MasterCredential')

class BetaBot {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request,response,auth }, next) {
    // call next to advance the request
    const user = auth.user
    const isCredentialCorrect = await CredentialChecker.runProperUserChecker(user,Ecosystem.LEDGER,[Authority.SANDBOX_BOT])
    if(!isCredentialCorrect) return response.forbidden({message:"You're not authorized to do this action"})
    await next()
  }
}

module.exports = BetaBot
