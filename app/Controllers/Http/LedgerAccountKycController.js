'use strict'

const KYC = use('App/Models/LedgerAccountKyc')
const Helpers = use('Helpers')
const User = use('App/Models/Credential/MasterUser')
const UserPrivilege = use('App/Models/Credential/SystemUserPrivilege')
const Database = use('Database')
const {Wallet} = use('ethers')
const MasterWallet = use('App/Models/WalletEthereum')

class LedgerAccountKycController {

  async index({request, response, view}) {

    const userList = await Database.raw("SELECT mu.id,mu.uuid, mu.email,lks.status ,lks.kyc_type FROM master_users mu join ledger_account_kycs lks on lks.user_id = mu.id join system_user_privileges sp on sp.user_id = mu.id where sp.privilegeGroupId not in (1) group by mu.uuid order by lks.updated_at asc ")
    return userList[0]

  }

  async groupByStatus({request, response, auth, params}) {
    const {query} = params
    const queryList = query.split(',')
    var queryString = queryList.map(query => "'" + query + "'")

    const listResult = await Database
      .raw("SELECT mu.uuid, mu.email,lks.status ,lks.kyc_type " +
        "FROM master_users mu " +
        "join ledger_account_kycs lks on lks.user_id = mu.id " +
        "where lks.status in (" + queryString + ")")
    return listResult[0]

  }

  async kycCountByStatus({request, response, auth}) {
    let pending = 0, notFinish = 0, valid = 0, refusedFrontId = 0, refusedBackId = 0, refusedProof = 0, banned = 0;
    const userCount = await Database.raw("SELECT mu.id,mu.uuid, mu.email,lks.status ,lks.kyc_type FROM master_users mu join ledger_account_kycs lks on lks.user_id = mu.id join system_user_privileges sp on sp.user_id = mu.id where sp.privilegeGroupId not in (1) group by mu.uuid order by lks.updated_at asc ")
    const userList = userCount[0]

    for (let i = 0; i < userList.length; i++) {
      const userStatus = userList[i].status
      if (userStatus === 'pending') pending++
      if (userStatus === 'valid') valid++
      if (userStatus === 'refused-front-id') refusedFrontId++
      if (userStatus === 'refused-back-id') refusedBackId++
      if (userStatus === 'refused-proof') refusedProof++
      if (userStatus === 'banned') banned++
      if (userStatus === 'not-finish') notFinish++

    }
    // const count = await KYC.query().select('status').count('id as count').groupBy('status')
    //
    // await count.map(async (data) => {
    //   data.status === 'pending' ? pending = data.count :
    //     data.status === 'valid' ? valid = data.count :
    //       data.status === 'refused-front-id' ? valid = data.count :
    //         data.status === 'refused-back-id' ? valid = data.count :
    //           false;
    // })
    return {pending, valid, refusedFrontId, refusedBackId, refusedProof, banned, notFinish}

  }

  async show({params, request, response, view, auth}) {
    const kyc = await KYC.findBy({user_id: auth.user.id})
    kyc.dob = new Date(kyc.dob).getFullYear() + "-" + (new Date(kyc.dob).getMonth()+1) + "-" + new Date(kyc.dob).getDate()
    return response.ok(kyc)
  }

  async showByIndex({params, request, response, view, auth}) {
    const {uuid} = params
    const client = await User.findBy({uuid})
    if (!client) return response.notFound({message: "Client not found"})
    const kyc = await KYC.findBy({user_id: client.id})
    if (!kyc) return response.notFound({message: "Kyc not found"})
    kyc.email = client.email
    return response.ok(kyc)
  }

  async store({request, response, auth}) {
    const kycRequest = request.only([
      'first_name',
      'last_name',
      'middle_name',
      'phone',
      'sex',
      'dob',
      'address',
      'city',
      'province',
      'nation',
      'zip_code',
      'kyc_type'
    ])

    const checkKYC = await KYC.findBy({user_id: auth.user.id})
    if (checkKYC) return response.forbidden({message: "You already have a KYC"})

    const kyc = await KYC.create(Object.assign({user_id: auth.user.id}, kycRequest))
    return response.ok(kyc)
  }

  async updateSelf({params, request, response, auth}) {

    const {first_name, last_name, middle_name, phone, sex, dob, address, city, province, nation, zip_code, kyc_type} = request.all()


    const kyc = await KYC.findBy({user_id: auth.user.id})

    if (!kyc) return response.notFound()

    kyc.first_name = first_name
    kyc.last_name = last_name
    kyc.middle_name = middle_name
    kyc.dob = dob
    kyc.phone = phone
    kyc.sex = sex
    kyc.address = address
    kyc.city = city
    kyc.province = province
    kyc.nation = nation
    kyc.zip_code = zip_code
    kyc.kyc_type = kyc_type

    await kyc.save()

    return response.ok(kyc)

  }

  async update({params, request, response, auth}) {

    const {first_name, last_name, middle_name, phone, sex, address, city, province, nation, zip_code, kyc_type} = request.all()

    const kyc = await KYC.findBy({user_id: params.id})

    if (!kyc) return response.notFound()

    kyc.first_name = first_name
    kyc.last_name = last_name
    kyc.middle_name = middle_name
    kyc.phone = phone
    kyc.sex = sex
    kyc.dob = dob
    kyc.address = address
    kyc.city = city
    kyc.province = province
    kyc.nation = nation
    kyc.zip_code = zip_code
    kyc.kyc_type = kyc_type

    await kyc.save()

    return response.ok(kyc)

  }

  async destroy({params, request, response}) {
  }

  async uploadFrontImage({request, response, auth, params}) {

    const idcard = request.file('pic_img')

    const customName = auth.user.uuid + "-front_image." + idcard.extname


    await idcard.move(Helpers.publicPath('kyc'), {
      name: customName,
      overwrite: true
    })

    if (!idcard.moved()) {
      return profilePic.error()
    }

    const clientKyc = await KYC.findBy({user_id: auth.user.id})
    clientKyc.front_image = customName
    await clientKyc.save()

    return response.ok({message: "Front image upload success"})
  }

  async uploadBackImage({request, response, auth, params}) {

    const idcard = request.file('pic_img')
    const customName = auth.user.uuid + "-back_image." + idcard.extname


    await idcard.move(Helpers.publicPath('kyc'), {
      name: customName,
      overwrite: true
    })

    if (!idcard.moved()) {
      return profilePic.error()
    }

    const clientKyc = await KYC.findBy({user_id: auth.user.id})
    clientKyc.back_image = customName
    await clientKyc.save()

    return response.ok({message: "Back image upload success"})
  }

  async uploadProveImage({request, response, auth, params}) {

    const idcard = request.file('pic_img')
    const customName = auth.user.uuid + "-prove_image." + idcard.extname


    await idcard.move(Helpers.publicPath('kyc'), {
      name: customName,
      overwrite: true
    })

    if (!idcard.moved()) {
      return profilePic.error()
    }

    const clientKyc = await KYC.findBy({user_id: auth.user.id})
    clientKyc.prove_image = customName
    clientKyc.status = "pending"
    await clientKyc.save()

    return response.ok({message: "Proven image upload success"})
  }

  async examineKyc({request, response, auth, params}) {
    const {uuid} = params
    const {status, note} = request.all()
    const client = await User.findBy({uuid})
    if (!client) return response.notFound({message: "Client not found"})
    const clientKyc = await KYC.findBy({user_id: client.id})
    if (!clientKyc) return response.badRequest({message: "Doesn't has KYC"})
    clientKyc.status = status
    clientKyc.note = note

    if (status === 'banned') await UserPrivilege.create({user_id: client.id, privilegeGroupId: 999})
    if (status !== 'valid') {
      const alreadyPayment = await UserPrivilege.findBy({user_id: client.id, privilegeGroupId: 3})
      if (alreadyPayment) await alreadyPayment.delete()
    }
    if (status === 'valid') {
      const alreadyPayment = await UserPrivilege.findBy({user_id: client.id, privilegeGroupId: 3})
      if (!alreadyPayment) await UserPrivilege.create({user_id: client.id, privilegeGroupId: 3})


      const wallet = new Wallet.createRandom()
      const address = wallet.signingKey.address

      const {mnemonic, privateKey, publicKey} = wallet.signingKey


      const alreadyHasWalet = await MasterWallet.findBy({'userId': client.id,})
      if (!alreadyHasWalet) {
        await MasterWallet.create({
          'userId': client.id,
          mnemonic,
          privateKey,
          publicKey,
          address,
        })
      }

    }

    await clientKyc.save()
    return {success: true}
  }

  async unBanUserAccount({request, response, auth, params}) {
    const {uuid} = params
    const client = await User.findBy({uuid})
    const clientKyc = await KYC.findBy({user_id: client.id})
    if (!clientKyc) return response.badRequest({message: "Doesn't has KYC"})
    clientKyc.status = "valid"
    const bannedNote = await UserPrivilege.findBy({user_id: client.id, privilegeGroupId: 999})
    const validPrevilege = await UserPrivilege.findBy({user_id: client.id, privilegeGroupId: 3})

    if (!bannedNote) return response.badRequest({message: "Ban note not found"})
    if (!validPrevilege) {
    }

    await bannedNote.delete()
    await clientKyc.save()
    await validPrevilege.delete()
    return {success: true}
  }


}

module.exports = LedgerAccountKycController
