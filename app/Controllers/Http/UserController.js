'use strict'
const KYC = use('App/Models/LedgerAccountKyc')
const Wallet = use('App/Models/WalletEthereum')
const {Ecosystem, Authority} = use('App/Helper/MasterCredential')

class UserController {

  async info({request, response, auth}) {
    let affiliate = false, admin = false, lender = false, valid = false
    const kyc = await KYC.findBy({user_id: auth.user.id})
    const walletAccount = await auth.user.wallet().fetch()

    const privilege = await auth.user.privilege().fetch()
    privilege.rows.map(privilege =>
      privilege.privilegeGroupId === Authority.CLIENT_V ? valid = true :
        privilege.privilegeGroupId === Authority.ADMIN_SU ? admin = true :
          privilege.privilegeGroupId === Authority.AFFILIATE_ACCOUNT ? affiliate = true :
            privilege.privilegeGroupId === Authority.LENDER_ACCOUNT ? lender = true : false
    )


    if (!kyc && !valid) {
      return response.ok(Object.assign(auth.user, {address: "", kyc: "undefined", pin: false}))
    }
    if (admin) {
      return response.ok(Object.assign(auth.user, {
          valid,
          wallet: false,
          affiliate,
          admin,
          lender,

        }))
    }

    return response.ok(Object.assign(auth.user,
      {
        address: kyc.status === 'pending' || kyc.status === 'not-finish'  ? "" : walletAccount.address,
        kyc: kyc.status ? kyc.status : "pending",
        pin: !walletAccount ? false : Boolean(walletAccount.pin),
        valid,
        wallet: valid,
        affiliate,
        admin,
        lender,

      }))
  }


}

module.exports = UserController
