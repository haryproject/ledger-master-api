'use strict'
const Transfer = use('App/Models/WalletTransfer')
const Payment = use('App/Models/PaymentOrder')
const Database = use('Database')

class LedgerAccountTransactionController {

  async ledgerHistory({request, response, auth}) {
    console.log("DO request")
    const wallet = await auth.user.wallet().fetch()
    if(!wallet) return response.notFound({message:"User's doesn't has any wallet, please finish your KYC Process"})

    const {address} = wallet
    const userTransactionLedger = await Database
      .raw("select lt.transaction_no,lt.sender,lt.receiver,lt.amount,lt.source,lt.type,lt.created_at,wt.status, " +
        "if(sender ='" + address + "','out','in') as direction " +
        "from  ledger_account_transactions lt " +
        "JOIN wallet_transfers wt on wt.transaction_number = lt.transaction_no " +
        "where sender = '" + address + "' " +
        "or receiver = '" + address + "' " +
        "order by lt.id desc")

    const transactionList = userTransactionLedger[0]

    return transactionList
  }

  async ledgerHistoryYear({request, response, auth, params}) {
    const {year} = params
    const wallet = await auth.user.wallet().fetch()
    if(!wallet) return response.notFound({message:"User's doesn't has any wallet, please finish your KYC Process"})

    const {address} = wallet
    const userTransactionLedger = await Database
      .raw("select lt.transaction_no,lt.sender,lt.receiver,lt.amount,lt.source,lt.type,lt.created_at,wt.status, " +
        "if(sender ='" + address + "','out','in') as direction " +
        "from  ledger_account_transactions lt " +
        "JOIN wallet_transfers wt on wt.transaction_number = lt.transaction_no " +
        "where (sender = '" + address + "' " +
        "or receiver = '" + address + "') " +
        "and year(lt.created_at) = "+year +" "+
        "order by lt.id desc")

    const transactionList = userTransactionLedger[0]


    return transactionList
  }

  async ledgerHistoryMonth({request, response, auth,params}) {
    const {year,month} = params
    const wallet = await auth.user.wallet().fetch()
    if(!wallet) return response.notFound({message:"User's doesn't has any wallet, please finish your KYC Process"})

    const {address} = wallet
    const userTransactionLedger = await Database
      .raw("select lt.transaction_no,lt.sender,lt.receiver,lt.amount,lt.source,lt.type,lt.created_at,wt.status, " +
        "if(sender ='" + address + "','out','in') as direction " +
        "from  ledger_account_transactions lt " +
        "JOIN wallet_transfers wt on wt.transaction_number = lt.transaction_no " +
        "where (sender = '" + address + "' " +
        "or receiver = '" + address + "') " +
        "and year(lt.created_at) = "+year +" "+
        "and month(lt.created_at) = "+month +" "+
        "order by lt.id desc")

    const transactionList = userTransactionLedger[0]

    return transactionList
  }

  async ledgerHistoryDay({request, response, auth,params}) {
    const {year,month,day} = params
    const wallet = await auth.user.wallet().fetch()
    if(!wallet) return response.notFound({message:"User's doesn't has any wallet, please finish your KYC Process"})

    const {address} = wallet
    const userTransactionLedger = await Database
      .raw("select lt.transaction_no,lt.sender,lt.receiver,lt.amount,lt.source,lt.type,lt.created_at,wt.status, " +
        "if(sender ='" + address + "','out','in') as direction " +
        "from  ledger_account_transactions lt " +
        "JOIN wallet_transfers wt on wt.transaction_number = lt.transaction_no " +
        "where (sender = '" + address + "' " +
        "or receiver = '" + address + "') " +
        "and year(lt.created_at) = "+year +" "+
        "and month(lt.created_at) = "+month +" "+
        "and day(lt.created_at) = "+day +" "+
        "order by lt.id desc")

    const transactionList = userTransactionLedger[0]


    return transactionList
  }


}

module.exports = LedgerAccountTransactionController
