'use strict'

class Login {
  get rules () {
    return {
      'email':'required|email',
      'password':'required'
    }
  }

  get messages(){
    return {
      'email.required':'Email must not empty',
      'password.required':'Password must not empty',

      'email.email':"Email's format is incorrect"
    }
  }
}

module.exports = Login
