'use strict'

class ExamineKyc {
  get rules () {
    return {
      status:'required|in:pending,refused-front-id,refused-back-id,refused-proof,banned,valid',
    }
  }

  get messages(){
    return {
      'status.required':"This field is required",
      'status.in':"Value must be between pending,refused-front-id,refused-back-id,refused-proof,banned,valid"
    }
  }


}

module.exports = ExamineKyc
