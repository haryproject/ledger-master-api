'use strict'

class Social {
  get rules () {
    return {
      key:'required'
    }
  }

  get messages(){
    return {
      'key.required':'Please provide an activation key'
    }
  }
}

module.exports = Social
