'use strict'

class EmailConfirmation {

  get rules () {
    return {
      'user':'required',
      'confirmation_link':'required',
      'receiver':'required|email',
      'subject':'required'
    }
  }

  get messages(){
    return {
      'user.required':'This field is required',
      'confirmation_link.required':'This field is required',
      'receiver.required':'This field is required',
      'subject.required':'This field is required',
      'receiver.email':'Incorrect email format'
    }
  }

}

module.exports = EmailConfirmation
