'use strict'

class UpdateEmail {
  get rules () {
    return {
      email:'required|email|unique,master_users:email'
    }
  }

  get messages(){
    return {
      'email.required':"Email's field is required",
      'email.email':'Email format is incorrect',
      'email.unique':'Email already taken'
    }
  }
}

module.exports = UpdateEmail
