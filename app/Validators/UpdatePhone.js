'use strict'

class UpdatePhone {
  get rules () {
    return {
      phone:'required'
    }
  }

  get messages(){
    return {
      'phone.required':"Phone's field is required",
    }
  }
}

module.exports = UpdatePhone
