'use strict'

class Kyc {
  get rules () {
    return {
      'first_name':"required",
      'last_name':'required',
      'middle_name':'required',
      'phone':'required',
      'sex':'required|in:male,female,undefined',
      'dob':'required|date',
      'address':'required',
      'city':'required',
      'nation':'required',
      'province':'required',
      'zip_code':'required',
      'kyc_type':'required|in:driving_license,passport,national_id',
    }
  }

  get messages(){
    return {
      'first_name.required':'First name must not be null',
      'last_name.required':'Last name must not be null',
      'middle_name.required':'Middle name must not be null',
      'phone.required':'Phone must not be null',
      'sex.required':'Sex must not be null',
      'address.required':'Address must not be null',
      'city.required':'City must not be null',
      'province.required':'Province must not be null',
      'nation.required':'Nation must not be null',
      'zip_code.required':'Zip code must not be null',
      'kyc_type.required':'Kyc type must not be null',
      'dob.required':'Date of birth is required',

      'dob.date':'Date of birth format invalid',

      'sex.in':'Sex must be between male, female or undefined',
      'kyc_type.in':'Kyc type must be between driving_license, passport, national_id'
    }
  }
}

module.exports = Kyc
