'use strict'

class UpdatePassword {
  get rules () {
    return {
      'key':'required',
      'oldPassword':'required',
      'newPassword':'required',
      'cNewPassword':'required|same:new_password'
    }
  }

  get messages(){
    return {
      'key.required':"Please provide a key",
      'oldPassword.required':"This field is required",
      'newPassword.required':"This field is required",
      'cNewPassword.required':"This field is required",
      'cNewPassword.same':'Password is not same'
    }
  }
}

module.exports = UpdatePassword
