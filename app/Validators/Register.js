'use strict'

class Register {
  get rules() {
    return {
      username: 'required|unique,master_users:username',
      email:'required|email|unique,master_users:email',
      password:'required'
    }
  }

  get messages(){
    return {
      'username.required':"Username must not empty",
      'email.required':"Username must not empty",
      'password.required':"Username must not empty",

      'email.email':"Email is bad formatted",

      'username.unique':'Username already taken',
      'email.unique':'Email already registered'
    }
  }
}

module.exports = Register
