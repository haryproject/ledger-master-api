'use strict'

class UploadKycImage {
  get rules () {
    return {
      // validation rules
      pic_img: 'required|file|file_ext:png,jpg|file_size:2mb|file_types:image'
    }
  }

  get messages(){
    return {
      'pic_img.required':'Please provide a file to be uploaded',
      'pic_img.file':'Please provide a file to be uploaded',
      'pic_img.file_ext':'Please provide .png .jpg file',
      'pic_img.file_size':'Please provide file with less than 2 Mb',
      'pic_img.file_types':'Please provide image file',
    }
  }
}

module.exports = UploadKycImage
