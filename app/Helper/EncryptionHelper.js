const Encryption = use('Encryption')
const Hash = use('Hash')


module.exports.encrypt = async function (data) {
  return Encryption.encrypt(data)
}

module.exports.decrypt = async function (data) {
  return  Encryption.decrypt(data)
}

module.exports.verify = async function (actual,encrypted) {
  return  await Hash.verify(actual,encrypted)
}

module.exports.hash = async function (data) {
  return  await Hash.make(data)
}

