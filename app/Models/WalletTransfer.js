'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class WalletTransfer extends Model {
  static get hidden(){
    return ['user_id','source','to','updated_at','id','receipt']
  }
}

module.exports = WalletTransfer
