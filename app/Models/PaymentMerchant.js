'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PaymentMerchant extends Model {

  static get hidden(){
    return ['created_at','id','updated_at','user_id','mnemonic','privateKey','publicKey']
  }
}

module.exports = PaymentMerchant
