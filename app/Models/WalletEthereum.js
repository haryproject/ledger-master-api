'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const {encrypt, decrypt,hash} = use('App/Helper/EncryptionHelper')

class WalletEthereum extends Model {
  static boot() {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (walletInstance) => {
      if (walletInstance.dirty.mnemonic) {
        walletInstance.mnemonic = await encrypt(walletInstance.mnemonic)
      }
      if (walletInstance.dirty.privateKey) {
        walletInstance.privateKey = await encrypt(walletInstance.privateKey)
      }
      if (walletInstance.dirty.publicKey) {
        walletInstance.publicKey = await encrypt(walletInstance.publicKey)
      }
    })
  }

  merchant(){
    return this.belongsTo('App/Models/PaymentMerchant','userId','user_id')
  }

  static get hidden(){
    return ['created_at','updated_at']
  }
}

module.exports = WalletEthereum
