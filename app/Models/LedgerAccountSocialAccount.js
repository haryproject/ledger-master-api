'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class LedgerAccountSocialAccount extends Model {
  static get hidden(){
    return ['created_at','id','user_id']
  }
}

module.exports = LedgerAccountSocialAccount
