'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class LedgerAccountKyc extends Model {

  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('afterFind', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.dob = new Date(userInstance.dob).getUTCDate()
      }
    })
  }

  static get hidden(){
    return ['created_at','updated_at','id']
  }
}

module.exports = LedgerAccountKyc
